// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TankAIController.generated.h"



class ATank;


/**
 * 
 */
UCLASS()
class MYBATTLETANK_API ATankAIController : public AAIController
{
	GENERATED_BODY()

	void BeginPlay() override;
	
	void Tick(float DeltaSeconds) override;

	

	void SetPawn(APawn* InPawn) override;

	
protected:
	//How close can the AI tank get
	UPROPERTY(EditAnywhere, Category="Setup")
		float AcceptanceRadius = 3000;
public:
	UFUNCTION()
		void OnDeath();
};
