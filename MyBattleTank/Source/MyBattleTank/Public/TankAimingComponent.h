// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"


UENUM()
	enum class EFiringState : uint8
{
	Reloading,
	Locked,
	Aiming,
	OutOfAmmunition
};

class UTankBarrel;
class UStaticMeshSocket;
class UTankTurret;
class AProjectile;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
	class MYBATTLETANK_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	bool IsBarrelMoving() const;
	// Sets default values for this component's properties
	UTankAimingComponent();
	EFiringState GetFiringState() const;
protected:
	// Called when the game starts
	void BeginPlay() override;
	UPROPERTY(BlueprintReadOnly, Category="State")
		EFiringState FiringState = EFiringState::Reloading;
public:
	void AimAt(FVector HitLocation);
	UFUNCTION(BlueprintCallable, Category="Firing")
		void Fire();

	UFUNCTION(BlueprintCallable, Category = "Input")
		void Initialise(UTankBarrel* BarrelToSet, UTankTurret* TurretToSet);

	UFUNCTION(BlueprintCallable, Category = "Firing")
		int32 GetAmmo() const;

	void MoveBarrelTowards(FVector AimDirection);
	UTankBarrel* Barrel = nullptr;
	UTankTurret* Turret = nullptr;


private:

	void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		TSubclassOf<AProjectile> ProjectileBlueprint;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
		float LaunchSpeed = 100000.f;
	UPROPERTY(EditDefaultsOnly, Category = "Firing")
		int32 Ammo = 3;
	UPROPERTY(EditAnywhere, Category = "Firing")
		float ReloadTimeInSeconds = 3.f;

	FVector AimDirection = {0, 0, 0};
	double LastFireTime = 0.0;
};
