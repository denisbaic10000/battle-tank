// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MYBATTLETANK_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()
public:
	UTankTrack();
	virtual void BeginPlay() override;
	void DriveTrack(float CurrentThrottle);

	UFUNCTION(BlueprintCallable)
		void SetThrottle(float Throttle);
	UPROPERTY(EditDefaultsOnly)
		float TrackMaxDrivingForce = 408000.f;

private:
	TArray<class ASprungWheel*> GetWheels() const;		
};
