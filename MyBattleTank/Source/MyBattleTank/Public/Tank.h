// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Tank.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeathDelegate);


UCLASS()
class MYBATTLETANK_API ATank : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATank();

	UFUNCTION(BlueprintPure, Category="Health")
		float GetHealthPercent() const;
	UPROPERTY()
		FDeathDelegate DeathDelegate;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	virtual  float TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent,AController * EventInstigator, AActor * DamageCauser) override;
	UPROPERTY(EditAnywhere, Category = "Gameplay")
		int32 StartHealth=100;
	UPROPERTY(VisibleAnywhere, Category="Gameplay")
		int32 CurrentHealth;

public:	
	
	// Called to bind functionality to input
	//virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;	
	
};
