// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"


class UTankAimingComponent;
class ATank;

/**
 * 
 */
UCLASS()
class MYBATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
		
	void SetPawn(APawn* InPawn) override;
protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
		void FoundAimingComponent(UTankAimingComponent* AimCompRef);
public:
	UPROPERTY(EditDefaultsOnly)
		float CrossHairXLocationX = 0.5f;
	UPROPERTY(EditDefaultsOnly)
		float CrossHairXLocationY = 0.333333f;	

		void BeginPlay() override;
	
		void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
		void AimTowardsCrosshair();
	UFUNCTION(BlueprintCallable)
		bool GetSightRayHitLocation(FVector& HitLocation) const;
	UFUNCTION(BlueprintCallable)
		bool GetLookDirection(FVector2D ScreenLocation, FVector& CameraDirection) const;
	UFUNCTION(BlueprintCallable)
		bool GetLookVectorHitLocation(FVector Direction, FVector& HitLocation, float LineTraceRange=100000000.f) const;
	UFUNCTION()
		void OnDeath();
};
