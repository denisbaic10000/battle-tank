// Fill out your copyright notice in the Description page of Project Settings.


#include "Public/TankTurret.h"

void UTankTurret::Rotate(float RelativeSpeed)
{
	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1.f, 1.f);

	const auto RotationChange = RelativeSpeed * MaxDegreesPerSecond*GetWorld()->DeltaTimeSeconds;
	//const auto NewRotation = RelativeRotation.Yaw + RotationChange;

	AddRelativeRotation({ 0,RotationChange,0 });
	//SetRelativeRotation({ 0, NewRotation,0 });
}
