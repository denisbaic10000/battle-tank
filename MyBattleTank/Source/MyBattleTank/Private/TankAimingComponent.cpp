// Fill out your copyright notice in the Description page of Project Settings.

#include "Public/TankAimingComponent.h"
#include "Public/TankBarrel.h"
#include "Public/TankTurret.h"
#include "Public/Projectile.h"
#include "Kismet/GameplayStatics.h"


bool UTankAimingComponent::IsBarrelMoving() const
{
	if (!ensure(Barrel))
		return false;

	return !AimDirection.Equals(Barrel->GetForwardVector(), 0.01f);
}

// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

EFiringState UTankAimingComponent::GetFiringState() const
{
	return FiringState;
}


// Called when the game starts
void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	LastFireTime = FPlatformTime::Seconds();
}


void UTankAimingComponent::AimAt(FVector HitLocation)
{
	if (!ensure(Barrel)) return;

	FVector OutLaunchVelocity, StartLocation = Barrel->GetSocketLocation(FName("Projectile"));

	const bool bHaveAimSolution = UGameplayStatics::SuggestProjectileVelocity(this,
	                                                                          OutLaunchVelocity,
	                                                                          StartLocation,
	                                                                          HitLocation,
	                                                                          LaunchSpeed,
	                                                                          false,
	                                                                          0, 0,
	                                                                          ESuggestProjVelocityTraceOption::
	                                                                          DoNotTrace,
	                                                                          FCollisionResponseParams::
	                                                                          DefaultResponseParam,
	                                                                          {GetOwner()});
	if (bHaveAimSolution)
	{
		AimDirection = OutLaunchVelocity.GetSafeNormal();

		MoveBarrelTowards(AimDirection);
	}
}

void UTankAimingComponent::Fire()
{
	if (FiringState == EFiringState::Aiming || FiringState == EFiringState::Locked)
	{
		if (!ensure(Barrel)) { return; }
		if (!ensure(ProjectileBlueprint)) { return; }

		AProjectile* Projectile = GetWorld()->SpawnActor<AProjectile>(ProjectileBlueprint,
		                                                              Barrel->GetSocketLocation(FName("Projectile")),
		                                                              Barrel->GetSocketRotation(FName("Projectile")));
		Projectile->LaunchProjectile(LaunchSpeed);
		LastFireTime = FPlatformTime::Seconds();
		Ammo--;
	}
}

void UTankAimingComponent::Initialise(UTankBarrel* BarrelToSet, UTankTurret* TurretToSet)
{
	Barrel = BarrelToSet;
	Turret = TurretToSet;
}

int UTankAimingComponent::GetAmmo() const
{
	return Ammo;
}

void UTankAimingComponent::MoveBarrelTowards(FVector _AimDirection)
{
	if (!ensure(Barrel && Turret))
		return;
	const auto BarrelRotator = Barrel->GetForwardVector().Rotation();
	const auto AimAsRotator = _AimDirection.Rotation();
	const auto DeltaRotator = AimAsRotator - BarrelRotator;

	Barrel->Elevate(DeltaRotator.Pitch);

	// Always Yaw the shortest way
	if (FMath::Abs(DeltaRotator.Yaw) < 180)
	{
		Turret->Rotate(DeltaRotator.Yaw);
	}
	else // Avoid going the long-way
	{
		Turret->Rotate(-DeltaRotator.Yaw);
	}
	//UE_LOG(LogTemp, Warning, TEXT("%f"), DeltaRotator.Pitch)
}

void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                         FActorComponentTickFunction* ThisTickFunction)
{
	if (Ammo <= 0)
	{
		FiringState = EFiringState::OutOfAmmunition;
	}
	else if ((FPlatformTime::Seconds() - LastFireTime) < ReloadTimeInSeconds)
	{
		FiringState = EFiringState::Reloading;
	}
	else if (IsBarrelMoving())
	{
		FiringState = EFiringState::Aiming;
	}
	else
	{
		FiringState = EFiringState::Locked;
	}
}
