// Fill out your copyright notice in the Description page of Project Settings.


#include "Public/TankBarrel.h"


void UTankBarrel::Elevate(float RelativeSpeed)
{
	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1.f, 1.f);
	const auto ElevationChange = RelativeSpeed * MaxDegreesPerSecond*GetWorld()->DeltaTimeSeconds;
	const auto RowNewElevation = RelativeRotation.Pitch + ElevationChange;

	SetRelativeRotation({ FMath::Clamp<float>(RowNewElevation,MinElevationDegree,MaxElevationDegree), 0,0 });
	//SetRelativeRotation({ RowNewElevation, 0,0 });
}
