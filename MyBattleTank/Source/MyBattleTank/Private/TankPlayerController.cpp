// Fill out your copyright notice in the Description page of Project Settings.


#include "Public/TankPlayerController.h"
#include "Public/TankAimingComponent.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Public/Tank.h"

void ATankPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if (!ensure(PossessedTank))
		{
			return;
		}
		PossessedTank->DeathDelegate.AddUniqueDynamic(this, &ATankPlayerController::OnDeath);
	}
}

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();	
	auto AimComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if (ensure(AimComponent))
		FoundAimingComponent(AimComponent);
}

void ATankPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	AimTowardsCrosshair();
	
}

void ATankPlayerController::AimTowardsCrosshair()
{
	if(!ensure(GetPawn()))
		return;
	FVector HitLocation;
	if(GetSightRayHitLocation(HitLocation))
	{
		GetPawn()->FindComponentByClass<UTankAimingComponent>()->AimAt(HitLocation);
	}
}

bool ATankPlayerController::GetSightRayHitLocation(FVector& HitLocation) const
{
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);

	const FVector2D ScreenLocation = { ViewportSizeX*CrossHairXLocationX, ViewportSizeY*CrossHairXLocationY };

	FVector LookDirection;
	if(GetLookDirection(ScreenLocation, LookDirection))
	{	
		return 	GetLookVectorHitLocation(LookDirection, HitLocation);
	}
	return false;
	
}

bool ATankPlayerController::GetLookDirection(FVector2D ScreenLocation, FVector& CameraDirection) const
{
	FVector tempWorldLocation;	
	return DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, tempWorldLocation, CameraDirection);
}

bool ATankPlayerController::GetLookVectorHitLocation(FVector Direction, FVector& HitLocation, float LineTraceRange) const
{
	FHitResult HitResult;

	const auto StartLocation = PlayerCameraManager->GetCameraLocation();
	const auto EndLocation = StartLocation + Direction * LineTraceRange;

	if(GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECollisionChannel::ECC_Camera))
	{
		HitLocation = HitResult.Location;		
		return true;
	}
	return false;
}

void ATankPlayerController::OnDeath()
{
	UE_LOG(LogTemp, Warning, TEXT("%s dead"), *GetName());
	StartSpectatingOnly();
}
