// Fill out your copyright notice in the Description page of Project Settings.


#include "Public/TankTrack.h"
#include "Public/SprungWheel.h"
#include "Public/SpawnPoint.h"

UTankTrack::UTankTrack()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UTankTrack::BeginPlay()
{
	Super::BeginPlay();
}

void UTankTrack::DriveTrack(float CurrentThrottle)
{
	const auto ForceApplied = CurrentThrottle*TrackMaxDrivingForce;
	auto Wheels = GetWheels();
	const auto ForcePerWheel = ForceApplied / Wheels.Num();
	for(auto Wheel: Wheels)
	{
		Wheel->AddDrivingForce(ForcePerWheel);
	}
}

void UTankTrack::SetThrottle(float Throttle)
{
	const float CurrentThrottle = FMath::Clamp<float>( Throttle,-1,1);
	DriveTrack(CurrentThrottle);
}

TArray<ASprungWheel*> UTankTrack::GetWheels() const
{
	TArray<ASprungWheel*> ResultArray;
	TArray <USceneComponent *> Children;
	GetChildrenComponents(true, Children);
	for(USceneComponent* Child : Children)
	{
		const auto SpawnPointChild = Cast<USpawnPoint>(Child);
		if(!SpawnPointChild)
			continue;
		AActor* SpawnedChild = SpawnPointChild->GetSpawnedActor();
		auto SprungWheel= Cast<ASprungWheel>(SpawnedChild);
		if(!SprungWheel) 
			continue;
		ResultArray.Add(SprungWheel);
	}
	return ResultArray;
}

