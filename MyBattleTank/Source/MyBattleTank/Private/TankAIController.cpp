// Fill out your copyright notice in the Description page of Project Settings.


#include "Public/TankAIController.h"
#include "Public/Tank.h"
#include "Engine/World.h" 
#include "Public/TankAimingComponent.h"
#include "GameFramework/PlayerController.h"



void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
}

void ATankAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	if (ensure(GetPawn()))
	{
		auto* ControlledTank = Cast<ATank>(GetPawn());

		auto* PlayerTank = Cast<ATank>(GetWorld()->GetFirstPlayerController()->GetPawn());
		if(PlayerTank)
		{
			auto* AimComp=ControlledTank->FindComponentByClass<UTankAimingComponent>();
			MoveToActor(PlayerTank, AcceptanceRadius);
			AimComp->AimAt(PlayerTank->GetActorLocation());
			if(AimComp->GetFiringState()==EFiringState::Locked)
				AimComp->Fire();
		}		
	}
}

void ATankAIController::OnDeath()
{
	UE_LOG(LogTemp, Warning, TEXT("%s dead"), *GetName());
	GetPawn()->DetachFromControllerPendingDestroy();
}


void ATankAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if(InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if(!ensure(PossessedTank))
		{
			return;
		}
		PossessedTank->DeathDelegate.AddUniqueDynamic(this, &ATankAIController::OnDeath);
	}
}
