// Fill out your copyright notice in the Description page of Project Settings.


#include "Public/Tank.h"
#include "Public/TankAimingComponent.h"
#include "Engine/World.h"
#include "Public/Projectile.h"
#include "Public/TankBarrel.h"
#include "Public/TankAIController.h"

float ATank::GetHealthPercent() const
{
	return float(CurrentHealth)/float(StartHealth);
}

// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	CurrentHealth = StartHealth;
}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = StartHealth;
	//DeathDelegate.AddUniqueDynamic(this, &ATankAIController::OnDeath);
}

float ATank::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	//Damage to apply
	DamageAmount = FMath::Clamp<float>(FPlatformMath::RoundToInt(DamageAmount), 0, CurrentHealth);
	CurrentHealth -= DamageAmount;
	if (CurrentHealth <= 0)
		DeathDelegate.Broadcast();
	return DamageAmount;
}


// Called to bind functionality to input
/*
void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
*/